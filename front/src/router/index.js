import Vue from 'vue'
import Router from 'vue-router'
import Homepage from '@/components/routes/Homepage'
import DonationSelector from '@/components/routes/DonationSelector'
import Checkout from '@/components/routes/Checkout'
import MyDonations from '@/components/routes/MyDonations'
import NotFound from '@/components/routes/NotFound'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'homepage',
      component: Homepage
    },
    {
      path: '/faire-un-don',
      name: 'donationSelector',
      component: DonationSelector
    },
    {
      path: '/faire-un-don/valider',
      name: 'checkout',
      component: Checkout
    },
    {
      path: '/mes-dons',
      name: 'myDonations',
      component: MyDonations
    },
    {
      path: '*',
      component: NotFound
    }
  ]
})
