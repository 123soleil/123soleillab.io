var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var secret = require('../config/secret');

var UserSchema = new mongoose.Schema({
  firstname: {
    type: String,
    required: [true, "can't be blank"],
    match: [/[A-zÀ-ÿ- ]+/, 'is invalid'],
    index: true
  },
  lastname: {
    type: String,
    required: [true, "can't be blank"],
    match: [/[A-zÀ-ÿ- ]+/, 'is invalid'],
    index: true
  },
  email: {
    type: String,
    lowercase: true,
    unique: true,
    required: [true, "can't be blank"],
    match: [/\S+@\S+\.\S+/, 'is invalid'],
  },
  adress: {
    type: String,
    lowercase: true,
    required: [true, "can't be blank"],
  },
  addressAddition: {
    type: String,
    lowercase: true,
    index: true
  },
  zipCode: {
    type: String,
    required: [true, "can't be blank"],
  },
  city: {
    type: String,
    required: [true, "can't be blank"],
  },
  country: {
    type: String,
    required: [true, "can't be blank"],
  },
  phone: {
    type: String,
    required: [true, "can't be blank"],
    match: [/\d{10}/, 'is invalid'],
  },
  password: String,
  hash: String,
  salt: String
}, { timestamps: true });

UserSchema.plugin(uniqueValidator, { message: 'is already taken.' });

UserSchema.methods.validPassword = function(password) {
  var hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  return this.hash === hash;
};

UserSchema.methods.setPassword = function(password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

UserSchema.methods.generateJWT = function() {
  var today = new Date();
  var exp = new Date(today);
  exp.setDate(today.getDate() + 60);
  return jwt.sign({
    id: this._id,
    username: this.username,
    exp: parseInt(exp.getTime() / 1000),
  }, secret);
};

UserSchema.methods.toJSON = function() {
  return {
    firstname: this.firstname,
    lastname: this.lastname,
    email: this.email,
    adress: this.adress,
    addressAddition: this.addressAddition,
    zipCode: this.zipCode,
    city: this.city,
    country: this.country,
    phone: this.phone,
  };
};

UserSchema.methods.toAuthJSON = function() {
  return {
    firstname: this.firstname,
    lastname: this.lastname,
    email: this.email,
    token: this.generateJWT(),
  };
};

module.exports = mongoose.model('User', UserSchema);
