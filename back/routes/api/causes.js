var mongoose = require('mongoose');
var router = require('express').Router();
var Cause = require('../../models/Cause');
var Donation = require('../../models/Donation');

router.post('/causes', (req, res, next) => {
  var cause = new Cause();
  cause.name = req.body.name;
  cause.description = req.body.description;
  cause.save().then(() => {
    return res.status(201).json({ cause: cause.toJSON() });
  }).catch(next);
});

router.get('/causes', (req, res, next) => {
  Cause.find().then((causes) => {
    return res.json({ causes: causes.map(cause => cause.toJSON()) });
  }).catch(next);
});

router.get('/causes/:id', (req, res, next) => {
  if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
    return res.sendStatus(404);
  }
  Cause.findById(req.params.id).then((cause) => {
    if (cause) {
      return res.json({ cause: cause.toJSON() });
    }
    return res.sendStatus(404);
  }).catch(next);
});

router.get('/causes/:id/donations', (req, res, next) => {
  if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
    return res.sendStatus(404);
  }
  Cause.findById(req.params.id).then((cause) => {
    if (!cause) {
      return res.sendStatus(404);
    }
    Donation.find().
    populate({
        path: 'receiver',
        match: { cause: cause._id }
    }).
    then(dons => {
      return res.json({
        donations: dons.filter(don => don.receiver).
          map(don => don.toJSON())
      });
    }).catch(next);
  }).catch(next);
});

module.exports = router;
