Local
Aider des causes autour de vous 
Environnemental
Agir pour pour la nature et les animaux
Education
Soutenir l'accès à la connaissance
Sante
Accompagner les personnes malades
Social
Lutter contre la pauvreté et la misère
Humanitaire
Secourir les peuples en détresse
Sciences
Financer la recherche scientifique