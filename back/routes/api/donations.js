var mongoose = require('mongoose');
var router = require('express').Router();
var auth = require('../auth.js');
var Donation = require('../../models/Donation');
var User = require('../../models/User');
var Organization = require('../../models/Organization');


router.post('/donations', auth.required, (req, res, next) => {
  User.findById(req.user.id).then((user) => {
    if (!user) { return res.sendStatus(401); }
    if (!req.body.donations) {
      return res.sendStatus(422).json({errors: {donations: "can't be blank"}});;
    }

    var promises = [];

    req.body.donations.map(donation => {
      if (!donation.type) {
        return res.sendStatus(422).json({errors: {type: "can't be blank"}});;
      }
      if (!mongoose.Types.ObjectId.isValid(donation.receiver)) {
        return res.sendStatus(422).json({errors: {receiver: "is invalid"}});
      }

      if (donation.type == "cause") {
        promises.push(Organization.find({ cause: donation.receiver }).then(orgs => {
          var numberOfOrgs = orgs.length;
          var amountToShare = donation.amount;
          var savePromises = []
          orgs.forEach(org => {
            var amount = Math.floor(amountToShare / numberOfOrgs)
            var don = new Donation();
            don.amount = amount;
            don.donor = user._id;
            don.receiver = org._id;
            numberOfOrgs--;
            amountToShare = amountToShare - amount;
            savePromises.push(don.save());
          })
          return Promise.all(savePromises);
        }));
      } else {
        var don = new Donation();
        don.amount = donation.amount;
        don.donor = user._id;
        don.receiver = donation.receiver;
        promises.push(don.save());
      }
    });

    Promise.all(promises).then((dons) => {
      //flatten dons since it is the result of several promises
      dons = dons.reduce((acc, val) => acc.concat(val), []);
      res.status(201).json({ donations: dons.map(don => don.toJSON()) });
    }).catch(next);

  }).catch(next);
});

router.get('/donations', auth.required, (req, res, next) => {
  User.findById(req.user.id).then((user) => {
    if (!user) { return res.sendStatus(401); }
    Donation.find({ donor: user._id }).
    populate('receiver').
    then(dons => {
      res.json({ donations: dons.map(don => don.toJSON()) });
    }).catch(next);
  }).catch(next);
});

module.exports = router;
