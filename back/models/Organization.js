var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var OrganizationSchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: [true, "can't be blank"],
    index: true
  },
  description: {
    type: String,
    required: [true, "can't be blank"],
  },
  logo: {
    type: String,
    required: [true, "can't be blank"],
  },
  cause: {
    type: mongoose.Schema.Types.ObjectId, ref: 'Cause',
    required: [true, "can't be blank"],
  }
});

OrganizationSchema.plugin(uniqueValidator, { message: 'is already taken.' });

OrganizationSchema.methods.toJSON = function() {
  return {
    id: this._id,
    name: this.name,
    description: this.description,
    logo: this.logo,
    cause: this.cause,
  };
};

module.exports = mongoose.model('Organization', OrganizationSchema);
