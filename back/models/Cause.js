var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var CauseSchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: [true, "can't be blank"],
  },
  description: {
    type: String,
    required: [true, "can't be blank"],
  }
});

CauseSchema.plugin(uniqueValidator, { message: 'is already taken.' });

CauseSchema.methods.toJSON = function() {
  return {
    id: this._id,
    name: this.name,
    description: this.description,
  };
};

module.exports = mongoose.model('Cause', CauseSchema);
